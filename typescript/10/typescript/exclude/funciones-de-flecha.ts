(function () {

    let miFuncion = function(a:string) {
        return a.toUpperCase();
    }

    const miFuncionFlecha = (a:string) => a.toUpperCase();
    
    console.log(miFuncion("Normal"));
    console.log(miFuncionFlecha("Flecha")); 


    const sumaNormal = function (a:number, b:number) {
        return a + b;
    }
    
    const sumaFlecha = (a:number, b:number) => a + b;
    console.log(sumaFlecha(2, 2)); 


    const hulk = {
        nombre: 'Hulk',
        smash () {
            setTimeout( () => {
                console.log(`${this.nombre} smash!!!`); 
            }, 1000);
        }
    }

    hulk.smash();
})();