(function () {

    /**
     * Funciones y tipos de parametros.
     * @param obligatorio Parametros obligatorios
     * @param opcional Parametros opcionales.
     * @param defecto Parametros por defecto.
     */
    function activar(obligatorio:string, opcional?:string, defecto:string = 'batiseñal') {
        console.log(`${obligatorio} activo la ${defecto} en la tarde`);
    }

    activar('Luis', 'tarde');

})();