(function () {

    function getEdad() {
        return 100 + 100;
    }
    
    const nombre = 'Luis';
    const appelido = 'Zepeda';
    const edad = 24;

    const salida = `${nombre} ${appelido} (${edad + 1}) - ${getEdad()}`;

    console.log(salida);
    
})();