import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { 
    console.log("Servicio listo");
  }

  getQuery (query: string) {

    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer QBP9WVVU-AlUdsuZr1MYZulLdwdY76o5__f3rjwJgbClSjQZ89hGWbaLnq89NEb9lmRThoO-I0s8RQ4bwk'
    });

    return this.http.get(url, {headers});
  }

  getNewReleases () {
    return this.getQuery('browse/new-releases?limit=20').pipe(map(response => response['albums'].items));
  }

  getArtistas (termino: string) {
    console.log("Termino de busqueda"+termino);
    return this.getQuery(`search?q=${termino}&type=artist&limit=15`).pipe(map(response => response['artists'].items));
  }

  getArtista (id: string) {
    return this.getQuery(`artists/${id}`)
    pipe(map(response => response['artists'].items));
    //.pipe(map(response => response['artists'].items));
  }

  getTopTracks (id: string) {
    return this.getQuery(`artists/${id}/top-tracks?country=us`).pipe(map(response => response['tracks']));
  }
}
