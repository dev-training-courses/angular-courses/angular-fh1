import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';
import { LoadingComponent } from '../shared/loading/loading.component';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: [
  ]
})
export class ArtistaComponent {

  artista: any = {};
  loading: boolean;
  tracks: any[] = [];

  constructor(private router: ActivatedRoute, private spotity: SpotifyService ) { 
    this.loading = true;
    this.router.params.subscribe(params => {
      this.getArtista(params['id']);
      this.getTracks(params['id']);
    });
  }

  getArtista(id: string) {
    this.spotity.getArtista(id)
    .subscribe(response => {  
      this.artista = response;
      this.loading = false;
    });
  }


  getTracks(id: string) {
    this.spotity.getTopTracks(id)
    .subscribe(response => {
      this.tracks = response;
    });
  }
  
}
