import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

	nombre:string = 'Capitán América';

	arreglo:number[] = [1,2,3,4,5,6,7,8,9,10];

	PI:number = Math.PI;

	procentaje:number = 0.1234;

	salario:number = 1234.5;

	heroe = {
		nombre : 'Logan',
		clave : 'Wolverine',
		edad : 500,
		direccion: {
			calle : 'Primera',
			casa : 20
		}
	}

	valorPromesa = new Promise<string>((resolve) => {
		setTimeout(() => {
			resolve('Llego la data');
		}, 4500);
	});

	fecha: Date = new Date();
	idioma:string = 'es';

	nombre2:string = 'luis zePeda';

	videoURL:string = 'https://www.youtube.com/embed/rrWRhrdwuLg';

	activar:boolean = true;
}
